#Monitoring Aufgabe Zusammen mit Ole bearbeitet
#Wir sind beide nicht besonders vertraut mit python oder Programmiersprachen allgemein daher möchte ich wie Ole das nicht bewertet haben



#psutil muss installiert sein
import psutil
import configparser
import time
import socket

#Funktion mit umrechnung der werte auslesbar über in Funktion definierte parameter
def get_disk_usage(path="C:/"):
    """
    :param path: Path of the disk
    :return: Dictionary -> rounded elements in GiB except percentage
    """
    disk_usage = psutil.disk_usage(path)
    total = round((disk_usage.total / 2**30), 2)
    used = round((disk_usage.used / 2**30), 2)
    free = round((disk_usage.free / 2**30), 2)
    percent = disk_usage.percent

    return {"total": total, "used": used, "free": free, "percent": percent}

#Funktion mit umrechnung der werte auslesbar über in Funktion definierte parameter
def get_virtual_memory():
    """
    :return: Dictionary -> rounded elements in GiB except percentage
    """
    total = round((psutil.virtual_memory().total / 2**30), 2)
    available = round((psutil.virtual_memory().available / 2**30), 2)
    percent = psutil.virtual_memory().percent
    used = round((psutil.virtual_memory().used / 2**30), 2)
    free = round((psutil.virtual_memory().free / 2**30), 2)

    return {"total": total, "available": available, "percent": percent, "used": used, "free": free}

#Funktion um log zu füllen
def write_log(typ, wert, limit):
    file = open("log.txt", "a")
    file.write("{} | {} | {} | {} | {} %\n".format(time.strftime("%d.%m.%y %H:%M:%S"), socket.gethostname(), typ, limit, wert))
    file.close()


config = configparser.ConfigParser()

config["SCHWELLENWERTE"] = {"Softlimit": 80,
                            "Hardlimit": 90}

file = open("conf.ini", "w")
config.write(file)
file.close()

softlimit = int(config["SCHWELLENWERTE"]["Softlimit"])
hardlimit = int(config["SCHWELLENWERTE"]["Hardlimit"])

run = True


#Schleife wiederholt sich nach ausführen ohne bedingung unendlich
while run:

    cpu = psutil.cpu_percent(interval=1)
    memory = get_virtual_memory()["percent"]
    disk = get_disk_usage()["percent"]
    prozesse = len(psutil.pids())

    # CPU
    if cpu >= softlimit and cpu < hardlimit:
        print("Warnung: Die Auslastung der CPU liegt bei", cpu, "%")
        write_log("CPU", cpu, "Softlimit")
    elif cpu >= hardlimit:
        print("Kritisch: Die Auslastung der CPU liegt bei", cpu, "%")
        write_log("CPU", cpu, "Hardlimit")

    # Arbeitsspeicher
    if memory >= softlimit and memory < hardlimit:
        print("Warnung: Die Auslastung vom Arbeitsspeicher liegt bei", memory, "%")
        write_log("Arbeitsspeicher", memory, "Softlimit")
    elif memory >= hardlimit:
        print("Kritisch: Die Auslastung vom Arbeitsspeicher liegt bei", memory, "%")
        write_log("Arbeitsspeicher", memory, "Hardlimit")

    # Festplatte
    if disk >= softlimit and disk < hardlimit:
        print("Warnung: Die Auslastung der Festplatte liegt bei", disk, "%")
        write_log("Festplatte", disk, "Softlimit")
    elif disk >= hardlimit:
        print("Kritisch: Die Auslastung der Festplatte liegt bei", disk, "%")
        write_log("Festplatte", disk, "Hardlimit")

    print("CPU-Auslastung:                  ", cpu, "%")
    print("Arbeitsspeicher-Auslastung:      ", memory, "%")
    print("Festplatten-Auslastung:          ", disk, "%")
    print("Anzahl laufender Prozesse:       ", prozesse)

    time.sleep(10)
